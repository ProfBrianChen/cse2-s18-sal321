// Jane Levin CSE2 Professor Chen Lab10
import java.util.Random; //importing random class 
public class lab10 { //class
  public static void main(String[] args) { //main method
    int firstHeight; //declaring int variables for the first and second heights and widths of the matrix
    int secondHeight; 
    int firstWidth; 
    int secondWidth; 
    firstHeight = (int)(Math.random()*5) + 2; //initializing the variables as random numbers 
    secondHeight = (int)(Math.random()*5) + 2;
    firstWidth = (int)(Math.random()*5) + 2; 
    secondWidth = (int)(Math.random()*5) + 2;
    int[][] A = new int[firstWidth][firstHeight]; //declaring and allocating space for two dimensional arrays A, B, and C
    int[][] B = new int[firstWidth][firstHeight];
    int[][] C = new int[secondWidth][secondHeight];
    A = increasingmatrix(firstWidth, firstHeight, true); //calling the increasingmatrix method for each array with a boolean format
    B = increasingmatrix(firstWidth, firstHeight, false);
    C = increasingmatrix(secondWidth, secondHeight, true);
    printmatrix(A, true); //calling the printmatrix method for each array with a boolean format 
    printmatrix(B, false);
    printmatrix(C, true);
    addmatrix(A, true, B, false); //calling the add matrix method for the two of the arrays with boolean format for each
    addmatrix(A, true, C, true);
  } //end of main method 
public static int[][] increasingmatrix(int width, int height, boolean format) { //declaring the increasingmatrix method 
  int[][] a = new int[width][height]; //declaring a new array with the same width and height 
  if(format) { //if format is true 
    int i = 0; //declaring and initializing integer i
    for (int k = 0; k < a.length; k++) { //for loop to run and determine elements of the new array with the same length 
      for (int j = 0; j < a[0].length; j++) { //for loop to run and determine elements of the second dimension of the new array with the same length 
        a[k][j] = i; //each element in the array equals an increasing number 
        i++; //increment i 
      }
    }
  }
  else{ //if format is false 
    int i = 0;  //declaring and initializing integer i 
    for (int k = 0; k<a[0].length; k++) { //for loop to run and determine elements of the second dimension of the new array with the same length
      for (int j = 0; j<a.length; j++) { //for loop to run and determine elements of the new array with the same length 
        a[j][k] = i; //each element in the array equals an increasing number 
        i++; //increment i 
      }
    }
  }
  return a; //return the array 
} //end of increasingmatrix method 
public static void printmatrix(int[][] array, boolean format) { //declaring the printmatrix method 
  if (array == null) { //if the array is empty 
    System.out.println("The array was empty."); //print statement 
  }
  else { //if the array is not empty 
    if (format) { //if format is true 
      System.out.println("Generating an array with a height of " + array.length + " and width of " + array[0].length + ": "); //print statement before generating array 
      for (int k = 0; k < array.length; k++) { //for loop to run the amount of times as the length of the array 
        System.out.print("[ "); //start of the print 
        for (int j = 0; j < array[0].length; j++) { //for loop to run the amount of times as the length of the second dimension of the array 
          System.out.print(array[k][j] + " "); //printing each element 
        }
        System.out.println("]"); //end of the printing of the array 
      }
    }
    else { //if format is false 
      System.out.print("The array was column major."); //print statement 
      int[][]newarray = new int[array.length][array[0].length]; //declaring and allocating space for a new array with the same lengths
      newarray = translate(array); //calling translate method to translate the array into column major 
    }
  }
} //end of printmatrix method 
public static int[][] translate(int[][] array) { //declaring the translate method 
  System.out.println("Translating column major into row major: "); //print statement before translating 
  int row = array.length; //declaring and initializing the row as the length of the array 
  int column = array[0].length; //declaring and initializing the column as the length of the second dimension of the array 
  int[][] newarray = new int[row][column]; //creating a new array with the same dimensions as the original array 
  for (int i = 0; i<row; i++) { //for loop running the amount of times as the rows of the array 
    for (int k = 0; k<array[i].length;k++) { //for loop running the amount of times as the columns of the array 
      newarray[i][k] = array[i][k]; //determining the elements of the new translated array compared to the original array 
    }
  }
  int [] linear = new int[row * column]; //new linear array with a length of the rows multiplied by the columns to create space for every element
  for (int i = 0; i<row; i++) { //for loop running the amount of times as the rows of the array 
    for (int k = 0; k<array[i].length; k++) { //for loop running the amount of times as the columns of the array 
      linear[(k+row)+i] = newarray[i][k]; //determining each element of the linear array by adding together the spaces on the loop with the row
    }
  }
  for (int i = 0; i<row; i++){ //for loop running the amount of times as the rows of the array 
    for (int k = 0; k<array[i].length; k++) { //for loop running the amount of times as the columns of the array 
      newarray[i][k] = linear[((i*column)+k)]; //determining each element of the new array by adding together the spaces on the loop within the linear array with the column 
    }
  }
  printmatrix(newarray, true); //calling the printmatrix method 
  return newarray; //returning the new array 
}  //end of translate method 
public static void addmatrix(int[][] a, boolean formatA, int[][] b, boolean formatB) { //declaring the addmatrix method 
  if (a.length != b.length || a[0].length != b[0].length) { //if the two arrays aren't the same lengths in their first or second dimensions
    System.out.println("The arrays cannot be added!"); //print that the arrays can't be added 
  }
  else if (a.length == b.length && a[0].length == b[0].length) { //if the lengths are the same in both dimensions
    if(formatA && !formatB) { //if the boolean for A is true and the boolean for B is false 
      b = translate(b); //calling the translate method for array b 
    }
    else if (formatB && !formatA) { //if the boolean for A is false and the boolean for B is true 
      a = translate(a); //calling the translate method for array a 
    }
    System.out.println("Adding the two matrices: "); //print statement before adding the matrices 
    for (int i = 0; i<a.length; i++) { //for loop running the amount of times as the length of the a array 
      System.out.print("[ "); //start of the printing of the array 
      for (int k = 0; k<a[0].length; k++) { //for loop running the amount of times as the length of the second dimension of the a array 
        System.out.print(a[i][k] + " "); //printing each element of the array 
      }
      System.out.println(" ]"); //end of the printing of the array 
    }
    System.out.println("Plus: "); //print statement before adding 
    for (int i = 0; i< b.length; i++) { //for loop running the amount of times as the length of the b array 
      System.out.print("[ "); //start of the printing of the array 
      for(int k = 0; k < b[0].length; k++) { //for loop running the amount of times as the length of the second dimension of the b array 
        System.out.print(b[i][k] + " "); //printing each element of the array 
      }
      System.out.println(" ]"); //end of the printing of the array 
    }
    int[][] finala = new int[a.length][a[0].length]; //declaring and allocating space for the final array with both added together 
    System.out.println("Equals: "); //print statement demonstrating what the two arrays equal when added 
    for (int i = 0; i < a.length; i++ ) { //for loop running the amount of times as the length of the a array 
      for (int k = 0; k < a[0].length; k++ ) { //for loop running the amount of times as the length of the second dimension of the a array 
        finala[i][k] = a[i][k] + b[i][k]; //initializing the element as equalling each element of the a and b array added together 
      }
    }
    printmatrix(finala, true); //calling the printmatrix method with the final array 
  }
} //end of the addmatrix method 
} //end of class 


