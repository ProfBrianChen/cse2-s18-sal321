Grade: 86.
Compiles without error and runs correctly (no runtime errors).
-10 for assigning numerical value to variables rather than assigning arithmetic calculations to the variables.
-4 for failure to correctly truncate decimals.
In the future, use appropriate units for output (in this case, $).
Remember to include name, class, Professor Chen, homework number and name as a comment at the top of your homework next time
(no points deducted for this missing comment this time).