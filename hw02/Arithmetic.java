public class Arithmetic {
  public static void main(String[] args) {
    //starting point for all java programs
    int numPants=3; //number of pants purchased
    double pantsPrice=34.98; //cost per pair of pants 
    int numShirts=2; //number of sweatshirts purchased
    double shirtPrice=24.99; //cost per sweatshirt
    int numBelts=1; //number of belts purchased
    double beltCost=33.99; //cost per belt
    double paSalesTax=0.06; //the tax rate in PA
    System.out.println("You paid "+(numPants*pantsPrice)+" for pants."); //printing out the calculation of the total cost for pants. 
    System.out.println("You paid "+(numShirts*shirtPrice)+" for sweatshirts."); //printing out the calculation of the total cost for sweatshirts. 
    System.out.println("You paid "+(numBelts*beltCost)+" for belts."); //printing out the calculation of the total cost for belts. 
    double totalPants=104.94; //the total amount paid for pants
    double totalShirts=49.98; //the total amount paid for sweatshirts
    double totalBelts=33.99; //the total amount paid for belts
    System.out.println("The tax is "+(totalPants*paSalesTax)+" for pants."); //printing out the calculation of the amount of tax on pants. 
    System.out.println("The tax is "+(totalShirts*paSalesTax)+" for shirts."); //printing out the calculation of the amount of tax on sweatshirts. 
    System.out.println("The tax is "+(totalBelts*paSalesTax)+" for belts."); //printing out the calculation of the amount of tax on sweatshirts. 
    double pantsTax=6.296; //the amount of tax on the pants
    double shirtTax=3.00; //the amount of tax on the sweatshirts
    double beltTax=2.04; //the amount of tax on the belts
    System.out.println("The total amount paid for clothes without tax is "+(totalPants+totalShirts+totalBelts)); //printing out the calculation of the price of clothing without tax. 
    double priceClothes=188.91; //the total price of clothes without tax
    System.out.println("The total amount of sales tax is "+(priceClothes*paSalesTax)); //printing out the calculation of the amount of tax on all of the clothes. 
    double totalTax=11.33; //the amount of tax on the entire shopping cart
    System.out.println("The total amount paid for clothes with tax is "+(priceClothes+totalTax)); //printing out the calculation of the price of clothing with tax.
    // The total amount paid for clothes with tax is 200.24 
  }
}