//Jane Levin CSE2 Professor Chen HW06
//This program displays an argyle pattern in text, asking the user for the dimensions as well as the characters.
import java.util.Scanner; //importing the scanner class
public class Argyle { //main method for every java program
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); //declaring the scanner class
    System.out.print("Enter a positive integer for the width of the viewing window in characters: "); //asking user for the width of the viewing window 
    while (!myScanner.hasNextInt()) {
      System.out.println("Error: Your input is invalid.");
      String junkWord = myScanner.next();
      System.out.print("Enter a positive integer for the width of the viewing window in characters: ");
    }
    int width = myScanner.nextInt();
    System.out.print("Enter a positive integer for the height of the viewing window in characters: "); //asking user for the height of the viewing window
    while (!myScanner.hasNextInt()) {
      System.out.println("Error: Your input is invalid.");
      String junkWord = myScanner.next();
      System.out.print("Enter a positive integer for the height of the viewing window in characters: ");
    }
    int height = myScanner.nextInt();
    System.out.print("Enter a positive integer for the width of the argyle diamonds: "); //asking the user for the width of the argyle diamonds
    while (!myScanner.hasNextInt()) {
      System.out.println("Error: Your input is invalid.");
      String junkWord = myScanner.next();
      System.out.print("Enter a positive integer for the width of the argyle diamonds: ");
    }
    int widthDiamonds = myScanner.nextInt();
    System.out.print("Enter a positive odd integer for the width of the argyle center stripe: "); //asking the user for the width of the argyle center stripes 
    while (!myScanner.hasNextInt()) {
      System.out.println("Error: Your input is invalid.");
      String junkWord = myScanner.next();
      System.out.print("Enter a positive odd integer for the width of the argyle center stripe: ");
    }
    int widthStripes = myScanner.nextInt();
    while (widthStripes > (widthDiamonds/2)) {
      System.out.println("Error: Your input is invalid.");
      String junkWord = myScanner.next();
      System.out.print("Enter a positive odd integer for the width of the argyle center stripe: ");
      widthStripes = new Scanner(System.in).nextInt();  
    }
    System.out.print("Enter a first character for the pattern fill: "); //asking the user for the character for the pattern fill
    String pattern1 = myScanner.next(); //declaring the input as a char
    char result5 = pattern1.charAt(0);
    char firstPattern = result5;
    System.out.print("Enter a second character for the pattern fill: "); //asking the user for the character for the second pattern fill
    String pattern2 = myScanner.next(); //declaring the input as a char
    char result6 = pattern2.charAt(0);
    char secondPattern = result6;
    System.out.print("Enter a third character for the stripe fill: "); //asking the user for the character for the stripe fill
    String pattern3 = myScanner.next(); //declaring the input as a char
    char result7 = pattern3.charAt(0);
    char thirdPattern = result7;
    for (int i = 0; i<width; i++) {
      for (int k = 0; k<height; k++) {
        if (i == k || k == (height * width - (i-1))) {
          System.out.print(firstPattern);
        } 
        if (i ==k || k == (((-height) * width) - (i-1))) {
          System.out.print(secondPattern);
        }
        if (i == k || k == ((height * (-width) - (i-1)))) {
          System.out.print(thirdPattern);
        }
        if (i == k || k == ((-height * width) - (i-1))) {
          System.out.print(thirdPattern);
          }
    }
      System.out.println();
    }
  } //end of main method
}  //end of class
      