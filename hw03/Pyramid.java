// Jane Levin CSE2 Professor Chen 
import java.util.Scanner; //importing the scanner class
public class Pyramid{ //main method for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //declaring an instance for the scanner object
    System.out.print("The square side of the pyramid is (input length): "); //inputting what the user will see
    int squareSide = myScanner.nextInt(); //allowing input of a value by the user 
    System.out.print("The height of the pyramid is (input height): "); //inputting what the user will see
    int height = myScanner.nextInt(); //allowing input of a value by the user
    int areaSquare; //declaring a new variable to represent the L * W of the square 
    areaSquare = squareSide * 25; //calculation to determine L * W
    int volume; //declaring a new variable to represent the volume of the pyramid
    volume = (areaSquare * height) / 3; //calculation to determine volume
    System.out.println("The volume inside the pyramid is " + volume + "."); //final print value for user to show volume
  }
}

