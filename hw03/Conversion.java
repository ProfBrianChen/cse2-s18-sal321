//Jane Levin CSE2 Professor Chen
import java.util.Scanner; //importing the scanner class. 
public class Conversion{ //main method for every Java program
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //declaring an instance for the scanner object
    System.out.print("Enter the affected area in acres: "); //inputting what the user will see
    double acres = myScanner.nextDouble(); //allowing input of a double value by user
    System.out.print("Enter the rainfall in the affected area in inches: "); //inputting what the user will see
    double inches = myScanner.nextDouble(); //allowing input of a double value by user
    double acresTimesInches;
    acresTimesInches = acres * inches;
    double gallons;
    gallons = acresTimesInches * 27154.3;
    double cubicMiles;
    cubicMiles = gallons * 9.08169e-13;
    System.out.println("The average amount of rainfall in cubic miles is: " + cubicMiles); //final print value to show user the total cubic miles.
  }
}

