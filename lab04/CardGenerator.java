//This program will use the random class to select a card with a specific suit and value. 
import java.util.Random; //importing the random class
public class CardGenerator { //main method for every java program
  public static void main(String[] args) {
    Random rand = new Random(); //declaring an instance for the random class
    int n = rand.nextInt(52) + 1; //setting the random class equal to a number between 1 and 52. 
    String suit; //declaring a variable for the suits
    suit = "";
    if ( n>=1 & n<=13 ){ //if statement declaring the suit type for a certain amount of cards.
      suit= "Diamonds";
    } 
    else if ( n>=14 & n<=26 ){ //if statement declaring the suit type for a certain amount of cards.
      suit= "Clubs"; n = n-13;
    } 
    else if ( n>=27 & n<=39 ){ //if statement declaring the suit type for a certain amount of cards. 
      suit= "Hearts"; n = n-26;
    } 
    else if ( n>=40 & n<=52 ){ //if statement declaring the suit type for a certain amount of cards. 
      suit= "Spades"; n = n-39;
    }
    String valueString; 
    switch (n){ //switch statement to assign a name to values 1, 11, 12, and 13. 
      case 1: valueString="Ace";
        break;
      case 11: valueString="Jack";
        break;
      case 12: valueString="Queen";
        break;
      case 13: valueString="King";
        break;
      default: valueString=""+n; //default case to declare that any other values will just be their integer value. 
        break;
    }
   System.out.println("You picked the "+valueString+" of "+suit+"."); //printing out what the user will see. 
  } //end of main method
} //end of class