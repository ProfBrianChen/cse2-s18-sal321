// Jane Levin CSE2 Professor Chen Lab06
//This program will print a "twist" on the screen depending on the length that the user inputs, and will ask for an integer. If not
//given an integer, then the code will loop until the user inputs an integer. 
import java.util.Scanner; //importing the scanner class
public class TwistGenerator{ //main method for every Java program
  public static void main (String [] args) {
    int length = 0; //declaring an integer variable length
    Scanner myScanner = new Scanner (System.in); //declaring the scanner class
    System.out.print("Enter a postiive integer called 'length': "); //printing what the user will see
    while (true) { 
      boolean test = myScanner.hasNextInt(); //stating that the input is true if it is an integer
      if (test == true) { 
        length = myScanner.nextInt(); //declaring the input as length if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't an integer
        System.out.print("Enter a positive integer called 'length': "); //looping again what the user will see if they did not input an integer.
      }
    }
    for (int x = 0; x < length; x++) { //declaring x as an integer and using it's relation to the length to start the first line of the twist
      if (x % 3 == 0) { //using a modulus to determine the meaning behind the length given by the user
        System.out.print("\\"); 
      } else if (x % 3 == 1) {
        System.out.print(" ");
      } else{
        System.out.print("/");
      }
    }
    System.out.println();
    for (int x = 0; x < length; x++) { //using the same pattern to determine the second line of the twist 
      if(x % 3 == 0) {
        System.out.print(" ");
      } else if(x % 3 == 1) {
        System.out.print("X");
      } else{
        System.out.print(" ");
      }
    }
    System.out.println();
    for (int x = 0; x < length; x++) { //using the same pattern to determine the third line of the twist
      if(x % 3 == 0) {
        System.out.print("/");
      } else if (x % 3 == 1) {
        System.out.print(" ");
      } else {
        System.out.print("\\");
      }
    }
    System.out.println();
  } //end of main method
} //end of class