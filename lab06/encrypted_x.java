//Jane Levin CSE2 Professor Chen Lab06
//Developing a program to hide the secret message X in stars. 
import java.util.Scanner; //importing the scanner class
public class encrypted_x { //main method for every java program
  public static void main (String[] args) {
    Scanner myScanner = new Scanner (System.in);
    int input; //declaring the input as an integer
    System.out.print("Input an integer from 0-100: "); //printing what the user will see
    while (!myScanner.hasNextInt()) { //while statement describing if the input is not an integer
      System.out.println("Error: Your input is invalid."); //printing what the user will see if their input is not an integer
      String junkWord = myScanner.next(); //getting rid of the input
      System.out.print("Input an integer from 0-100: "); //printing what the user will see again to loop it back again
    }
    input = myScanner.nextInt(); //initializing the input 
    while (input > 100 || input < 0) { //while statement describing if the input is greater than 100 or less than 0.
      System.out.println("Error: Your input is invalid"); //printing what the user will see if their input is greater than 100 or less than 0.
      String junkWord = myScanner.next(); //getting rid of the input
      System.out.print("Input an integer from 0-100: "); //printing what the user will see again to loop it back again
      input = new Scanner(System.in).nextInt(); //initializing the input again
   }
    for (int i = 0; i<input; i++) { //initializing a variable i to describe the length of the printed X code
      for (int k = 0; k<input; k++) { //initializixng a variable k to describe the width of the printed X code
        if (i==k || k == (input - (i-1))) { //if statement to compare the two variables to the input 
          System.out.print(" "); //printing what the user will see
        } else {
          System.out.print("*");
        }
      } 
      System.out.println();
    } 
  } //end of main method
} //end of class
