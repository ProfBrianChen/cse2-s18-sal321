// Jane Levin HW06 Professor Chen [CSE2] 
// This program asks the user to give information about a course that they are taking and runs infinite loops unless the user inputs the correct type. 
import java.util.Scanner; //importing the scanner class
public class Hw06 { //main method of every java program
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in); //declaring the scanner class
    System.out.print("Enter the course number of your class: "); //printing what the user will see
    while (true) { 
      boolean test = myScanner.hasNextInt(); //stating that the input is true if it is an integer
      if (test == true) { 
        int courseNum = myScanner.nextInt(); //declaring the input as the course number if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't an integer
        System.out.println("Error: You must input an integer type."); //message shown if input is false
        System.out.print("Enter the course number of your class: "); //looping again what the user will see if they did not input an integer.
      }
    }
    System.out.print("Enter the department name of your class: "); //printing what the user will see
    while (true) {
      boolean test2 = myScanner.hasNext(); //stating that the input is true if it is a string
      if (test2 == true) {
        String courseDept = myScanner.next(); //declaring the input as the course department if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't a string
        System.out.println("Error: You must input a string type."); //message shown if input is false
        System.out.print("Enter the department name of your class: "); //looping again what the user will see if they did not input a string
      }
    }
    System.out.print("Enter the number of times your class meets per week: "); //printing what the user will see 
    while (true) {
      boolean test3 = myScanner.hasNextInt(); //stating that the input is true if it is an integer
      if (test3 == true) {
        int courseAmount = myScanner.nextInt(); //declaring the input as the amount of times the course meets if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't an integer
        System.out.println("Error: You must input an integer type."); //message shown if input is false
        System.out.print("Enter the number of times your class meets per week: "); //looping again what the user will see if they did not input an integer
      }
    }
    System.out.print("Enter the time that your class starts: "); //printing what the user will see
    while (true) {
      boolean test4 = myScanner.hasNextDouble(); //stating that the input is true if it is a double
      if (test4 == true) {
        double courseTime = myScanner.nextDouble(); //declaring the input as the time of the course if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't a double
        System.out.println("Error: You must input a double type."); //message shown if input is false 
        System.out.print("Enter the time that your class starts: "); //looping again what the user will see if they did not input a double
      }
    }
    System.out.print("Enter the instructor's name of your course:"); //printing what the user will see
    while (true) {
      boolean test5 = myScanner.hasNext(); //stating that the input is true if it is a string
      if (test5 == true) {
        String courseInstructor = myScanner.next(); //declaring the input as the course instructor if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't a string
        System.out.println("Error: You must input a string type."); //message shown if input is false
        System.out.print("Enter the instructor's name of your course: "); //looping again what the user will see if they did not put a string
      }
    }
    System.out.print("Enter the number of people in your class: "); //printing what the user will see
    while (true) {
      boolean test6 = myScanner.hasNextInt(); //stating that the input is true if it is an integer
      if (test6 == true) {
        int courseClass = myScanner.nextInt(); //declaring the input as the number of people in the class if it is true
        break;
      } else {
        String junkWord = myScanner.next(); //getting rid of the input if it isn't an integer
        System.out.println("Error: You must input an integer type."); //message shown if input is false
        System.out.print("Enter the number of people in your class: "); //looping again what the user will see if they did not input an integer
      }
    }
  } //end of main method
} //end of class