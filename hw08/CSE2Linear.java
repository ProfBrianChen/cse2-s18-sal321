//Jane Levin CSE2 Professor Chen HW08
//Generating a program with arrays that will scramble integers and will use a linear and binary search to look for a user's input
import java.util.Scanner; //importing the scanner class
import java.util.Arrays; //importing the arrays class 
public class CSE2Linear{ //class 
  public static void main (String[] args){ //main method 
    System.out.println("Enter 15 integers in ascending order for final grades in CSE2: "); //printing what the user will see 
    Scanner myScanner = new Scanner(System.in);
    int[] grades = new int[15]; //declaring an array with 15 elements called grades
    for (int i = 0; i<15; i++){ //for loop determining position and how many times it will run 
      if (myScanner.hasNextInt()){ //if the input is an integer
        grades[i] = myScanner.nextInt(); //put the input into the i position of the array 
        if (grades[i] < 0 || grades[i] > 100) { //if the input is less than 0 or greater than 100
          System.out.println("Error: Your integer is in not in the proper range."); //error message for not being in the proper range
          System.exit(0); //exit the loop
        } else { 
          if (i > 0) { //if the position is greater than the first one
            if (grades[i] < grades[i-1]){ //if the current i value is less than the value of the element in the position before
              System.out.println("Error: Your integer is not greater than or equal to the last one."); //error message for not ascending order
              System.exit(0); //exit the loop 
            }
          }
        }
      }
      else{
        System.out.println("Error: Your input is not an integer."); //error message if input is not an integer
        System.exit(0); //exit the loop 
      }
    }
    System.out.println("Sorted Grades: "); //printing sorted grades
    System.out.println(Arrays.toString(grades)); //printing the array of grades 
    System.out.print("Enter a grade to search for: "); //printing what the user will see to input a grade to search for 
    if (myScanner.hasNextInt()){ //if the input for a grade to search for is an integer
      int k = myScanner.nextInt(); //declaring the input as an int 
      BinarySearch(grades, k); //calling the method for a binary search 
      Scramble(grades); //calling the method for a scramble 
      System.out.println("Scrambled Grades: "); //printing what the user will see before the scrambled grades
      System.out.println(Arrays.toString(grades)); //printing the new array of grades
      System.out.print("Enter a grade to search for: "); //printing what the user will see to input another grade to search for 
      if (myScanner.hasNextInt()){ //if the input for a grade to search for is an integer
        LinearSearch(grades, k); //calling the linear search method 
      }
      else if (!myScanner.hasNextInt()){
        System.out.println("Error: Your input is not an integer."); //error message if input is not an integer
        System.exit(0); //exit the loop 
      }
    }
    else{
      System.out.println("Error: A grade was not entered."); //error message if nothing was entered/ it is wrong. 
      System.exit(0); //exit the loop 
    }
  } //end of main method 
    public static void BinarySearch(int[] grades2, int search){ //declaring the binarysearch method 
    int max = grades2.length - 1; //maximum integer is equal to the length of the array - 1
    int min = 0; //minimum integer declared and initialized as 0. 
    int counter = 0; //initializing and declaring a counter. 
    while (min <= max) { //while the minimum is less than or equal to the max
      counter++; //increment the counter
      int mid = (max + min) / 2; //declaring a middle integer that is equal to the average of the min and max. 
      if (search < grades2[mid]){ //if the integer that the user is looking for is less than the element in the middle position of the array 
        max = mid - 1; //max will decrease 
      } else if (search > grades2[mid]){ //if the integer that the user is looking for is greater than the element in the middle position of the array 
        min = mid + 1; //max will increase
      } else if (search == grades2[mid]){ //if the integer is equal to the middle position of the array 
        System.out.println(search + " was found in " + counter + " iterations."); //printing that it was found and the number of iterations is the counter's value. 
        break; 
      }
    }
    if (min > max) { //if the minimum is eventually greater than the maximum 
      System.out.println(search + " was not found in " + counter + " iterations"); //printing that it was not found and the number of iterations is the counter's value. 
    }
  } //end of binarysearch method 
  public static int[] Scramble(int[] grades) { //declaring the scramble method 
    for (int j = 0; j < grades.length; j++) { //for loop to determine the number of times it should run and the position on the array 
      int k = (int)(Math.random()*grades.length); //generating a random number for the int k to be a position in the array 
      int array = grades[j]; //declaring and initializing an int to represent each grade on the array 
      while (k != j){ //when the random position is not equal to the position on the original grades array 
        grades[j] = grades[k]; //make them equal to one another to change the position 
        grades[k] = array; //change the value on the new, scrambled array 
      }
    }
    return (grades); //returning the grades array 
  } //end of scramble method 
    public static void LinearSearch(int[] grades, int k){ //declaring the linearsearch method 
    for (int j = 0; j < grades.length; j++) { //for loop to determine the number of times it should run and the position on the array 
      if (grades[j] == k){ //if the element in the position of the specific loop is equal to the input of the user 
        System.out.println(k + " was found in the list in " + (j + 1) + " iterations."); //print statement for if the grade was found in the number of iterations that the loop ran. 
        break;
      } else if (grades[j] != k && k == (grades.length - 1)) { //if the element in the position of the specific loop is not equal to the input of the integer and if k is equal to the length-1, therefore reaching the end 
        System.out.println(k + " was not found in the list in " + (j + 1) + " iterations."); //print statement for if the grade was not found in the number of iterations that the loop ran. 
      }
    }
  } //end of linearsearch method 
} //end of class 