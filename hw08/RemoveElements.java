//Jane Levin CSE2 Professor Chen HW08
//Generating a program that will randomly create an array of 10 integers between 0 and 9, delete an element in the position that the user inputs,
//and then remove the elements of the array that are equal to the user's input. 
import java.util.Scanner; //importing the scanner class
public class RemoveElements{ //main class 
  public static void main(String [] arg){ //main method 
	Scanner scan=new Scanner(System.in); //declaring scanner
int num[]=new int[10]; //declaring an array called num
int newArray1[]; //declaring an array called newArray1
int newArray2[]; //declaring an array called newArray2
int index,target; //declaring two integers
	String answer=""; //declaring a string
	do{ //start of a do while loop 
  	System.out.print("Random input 10 ints [0-9]"); //printing what the user will see 
  	num = randomInput(); //calling the randomInput method to generate random numbers 
  	String out = "The original array is:"; //printing the phrase
  	out += listArray(num); 
  	System.out.println(out); //printing the array 
 
  	System.out.print("Enter the index "); //printing what the user will see to input the index
  	index = scan.nextInt(); //initializing the index as the input 
  	newArray1 = delete(num,index); //calling the delete method 
  	String out1="The output array is "; //printing the phrase 
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1); //printing the new array with the index position deleted 
 
      System.out.print("Enter the target value "); //printing what the user will see to input the target
  	target = scan.nextInt(); //initializing the target as this input 
  	newArray2 = remove(num,target); //calling the remove method 
  	String out2="The output array is "; //printing the phrase
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2); //printing the new array with all elements of the array with the target positions removed
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-"); //asking the user whether or not they'd like to run it again 
  	answer=scan.next(); 
	}while(answer.equals("Y") || answer.equals("y")); //loop again if the user inputs Y or y
  } //end of main method
  public static String listArray(int num[]){ //declaring the listArray method 
	String out="{"; //initializing a string value 
	for(int j=0;j<num.length;j++){ //for loop to determine how many times it will run 
  	if(j>0){ //if statement that won't run the first time around 
    	out+=", "; //adding a comma between the elements of the array 
  	}
  	out+=num[j]; //adding a new element of the array 
	}
	out+="} "; //adding the end of the curly bracket to the array 
	return out; //returning the entire list of values with commas and brackets
  }
  public static int[] randomInput(){ //declaring the randomInput method 
    int[] integers = new int[10]; //declaring a new array called integers with 10 spaces
    for (int k = 0; k<10; k++) {  //for loop to determine how many times it will run 
      integers[k] = (int)(Math.random() *9); //randomly generating the array with elements between 0 and 9 
  }
     return (integers); //returning the array 
 }
  public static int[] delete(int []list, int pos) { //declaring the delete method 
     int[] newlist = new int[9]; //declaring a new array called newlist with 9 spaces
     int j = 0; //declaring and initializing a new variable j
     for (int i = 0; i<9; i++){ //for loop to determine how many times it will run and the position on the array 
       if (i < pos) { //if i is less than the user input of the position that they want to delete
         newlist[j] = list[i]; //the new list will have the same element in the same position as the old list  
         j++; //incremenet j 
       } else if (i >= pos) { //else if i is greater than or equal to the user input of the position that they want to delete
           newlist[j] = list[i+1]; //the new list will have the same element in the position that is one ahead of the old list. 
				   j++; //increment j 
        }
      }
	   return (newlist); //returning the new list array 
    }
  public static int[] remove(int []list, int target) { //declaring the remove method 
    int[] newlist2 = new int[list.length]; //declaring a new array called newlist2 with the length of the inputted list. 
	  int j = 0; //initializing and declaring two variables as 0. 
		int z = 0;
	  for (int i = 0; i<list.length; i++) { //for loop determining how many times it will run and the position on the array 
       if (list[i] != target) { //if the list's element is not equal to the value that the user inputs
          newlist2[j] = list[i]; //the new list will have the same element with the same position as the inputted array. 
				  j++; //increment j 
      } else if (list[i] == target) { //if the list's element is equal to the value that the user inputs 
         z++; //increment z 
      }
    }
		int []newlist3 = new int[list.length - z]; //declaring a new array called newlist3 with the length of the inputted list minus z, which is equal to the number of elements equal to the target value. 
		for (int k = 0; k < list.length-z; k++) { //for loop determining how many times it will run 
			newlist3[k] = newlist2[k]; //new list 3 is equal to new list 2 with the same positions and elements, yet a different length. 
		}
    return (newlist3); //returning newlist3 array
  }
} //end of class
