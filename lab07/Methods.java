//Jane Levin Lab07 CSE2 Professor Chen
//Artificial story generation
import java.util.Random;
import java.util.Scanner;//importing the Random and Scanner class
public class Methods{
  public static String adj; //initializing methods for the parts of speech
  public static String nounsub;
  public static String ptverb;
  public static String nounobj;
  public static void main (String[] args){ //main method for every java program
    adj = AdjectiveClass(); //declaring the methods as a calling of the latter methods for the parts of speech
    nounsub = nounSubject();
    ptverb = pastTenseVerbs();
    nounobj = nounObject();
    Sentence1(); //calling sentence one to the main method
    Scanner myScanner = new Scanner(System.in);
    System.out.print("Would you like to add another sentence? Press 0 to conclude the story, press 1 to add more: ");
    int choice = myScanner.nextInt();
    while(choice == 1) {
     adj = AdjectiveClass(); //calling the parts of speech again besides the nounSubject so that they are different for each sentence. 
     ptverb = pastTenseVerbs();
     nounobj = nounObject();
     Sentence2(); //calling sentence 2
     System.out.print("Would you like to add another sentence? Press 0 to finish, press 1 to add another: ");
     choice = myScanner.nextInt();
    }
    if(choice == 0) {
      adj = AdjectiveClass(); //calling the parts of speech again besides the nounSubject so that they are different in the conclusion sentence. 
      ptverb = pastTenseVerbs();
      nounobj = nounObject();
      Sentence3(); //calling the conclusion sentence
    }
  } //end of main method
    public static String AdjectiveClass(){ //declaring the method for the adjective
      Random randomGenerator = new Random (); 
      int random1 = randomGenerator.nextInt(10); //generating a random integer from 0-9
      String Adjective = " "; //initializing and declaring the adjective String value
      switch(random1) { //switch statement for the result of the random integer 
        case 0: Adjective = "fast";
          break;
        case 1: Adjective = "clean";
          break;
        case 2: Adjective = "funny";
          break;
        case 3: Adjective = "confusing";
          break;
        case 4: Adjective = "helpful";
          break;
        case 5: Adjective = "happy";
          break;
        case 6: Adjective = "sad";
          break;
        case 7: Adjective = "grumpy";
          break;
        case 8: Adjective = "large";
          break;
        case 9: Adjective = "silent";
          break;
      }
      return Adjective; //return statement for the Adjective variable. 
    } //end of method
    public static String nounSubject(){ //declaring the method for the nounSubject
      Random randomGenerator = new Random ();
      int random2 = randomGenerator.nextInt(10); //generating a random integer from 0-9
      String Noun1 = " "; //declaring an initializing the noun string value
      switch(random2) { //switch statement for the result of the random integer
        case 0: Noun1 = "clock";
          break;
        case 1: Noun1 = "dog";
          break;
        case 2: Noun1 = "food";
          break;
        case 3: Noun1 = "athlete";
          break;
        case 4: Noun1 = "woman";
          break;
        case 5: Noun1 = "blanket";
          break;
        case 6: Noun1 = "shirt";
          break;
        case 7: Noun1 = "deer";
          break;
        case 8: Noun1 = "window";
          break;
        case 9: Noun1 = "computer";
          break;
      }
      return Noun1; //return statement for the noun1 variable
    } //end of method
    public static String pastTenseVerbs(){ //declaring the method for the past tense verbs
      Random randomGenerator = new Random (); 
      int random3 = randomGenerator.nextInt(10); //generating a random integer from 0-9 
      String Verb = " "; //declaring and initializing the verb string value
      switch(random3) { //switch statement for the result of the random integer
        case 0: Verb = "ran";
          break;
        case 1: Verb = "ate";
          break;
        case 2: Verb = "cried";
          break;
        case 3: Verb = "laughed";
          break;
        case 4: Verb = "smiled";
          break;
        case 5: Verb = "danced";
          break;
        case 6: Verb = "sang";
          break;
        case 7: Verb = "gathered";
          break;
        case 8: Verb = "moved";
          break;
        case 9: Verb = "watched";
          break;
      }
      return Verb; //return statement for the verb variable
    } //end of method
    public static String nounObject(){ //declaring the method for the nounObject
      Random randomGenerator = new Random (); 
      int random4 = randomGenerator.nextInt(10); //generating a random integer from 0-9
      String Noun2 = " "; //declaring and initializing the noun string value
      switch(random4) { //switch statement for the result of the random integer
        case 0: Noun2 = "shoes";
          break;
        case 1: Noun2 = "man";
          break;
        case 2: Noun2 = "flowers";
          break;
        case 3: Noun2 = "candy";
          break;
        case 4: Noun2 = "bear";
          break;
        case 5: Noun2 = "bird";
          break;
        case 6: Noun2 = "curtain";
          break;
        case 7: Noun2 = "bag";
          break;
        case 8: Noun2 = "ball";
          break;
        case 9: Noun2 = "picture";
          break;
      }
      return Noun2; //return statement for the noun2 variable
    } //end of method
  public static void Sentence1(){ //delcaring the method for the first "thesis" sentence
    System.out.println("The "+adj+" "+nounsub+" "+ptverb+" with "+"the "+nounobj+"."); //printing the format of the sentence with the variables.
  } //end of method
  public static void Sentence2(){ //declaring the method for the middle sentences
    String sub = " "; //declaring and initializing the subject of the sentence 
    Random randomGenerator = new Random ();
    int random6 = randomGenerator.nextInt(2); //generating a random integer from 0-1
      if (random6==0) { //if statement for the random number being 0, in which the subject will print "it"
        sub = "It ";
      } else { //else statement for the random number being 1, in which the subject will print itself. 
        sub = ("The " + nounsub + " ");
      }
      System.out.println(sub+ptverb+" when the "+nounobj+ " was "+adj+"."); //printing the format of the middle sentences
    } //end of method
  public static void Sentence3(){ //declaring the method for the conclusion sentence
    System.out.println("That "+nounsub+" "+ptverb+ " the "+nounobj+"!"); //printing the format of the conclusion sentence
  } //end of method 
  } //end of class

