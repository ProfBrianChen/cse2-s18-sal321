//Jane Levin CSE2 Professor Chen HW10
//This program develops a city and prints the populations of each block and the result of robot invasions through multidimensional arrays. 
import java.util.Random; //importing random class 
import java.util.Arrays; //importing arrays
public class RobotCity{ //class 
  public static void main (String[] args){ //main method
    int[][] cityArray = buildCity(); //declaring cityArray as the result of the method buildCity
    for (int i = 0; i<5; i++){ //for loop to run 5 times
      display(cityArray); //calling the display method for the cityArray
      System.out.println(); 
      int invading = (int)(Math.random()*5); //generating a random number from 0 to 5 demonstrating the number of robots invading 
      invade(cityArray, invading); //calling the invade class for the cityArray and random integer 
      display(cityArray); //calling the display method for the cityArray
      System.out.println();
      update(cityArray); //calling the update method for the cityArray
      display(cityArray); //calling the display method again for the updated cityArray
      System.out.println();
    } //end of for loop 
  } //end of main method 
  public static int[][] buildCity(){ //declaring the buildCity method as a 2 dimensional int array 
    int rows = (int)(Math.random()*(16-10) + 10); //generating a random number from 10-15 to represent the number of rows 
    int columns = (int)(Math.random()*(16-10) + 10); //generating a random number from 10-15 to represent the number of columns 
    int[][] array = new int[rows][columns]; //initializing an array to have the number of rows and columns generated 
    for (int i = 0; i<array.length; i++){ //for loop for the number of rows 
      for (int k = 0; k<array[i].length; k++){ //for loop for the columns 
        array[i][k] = (int)(Math.random()*(1000-100) + 100); //allocating a number from 100-999 to represent the population of each element of the array 
      }
    }
    return array; //return statement
  } //end of buildCity method 
  public static void display(int[][] array){ //declaring the display method as void and accepting a 2d int array 
    for (int i =0; i<array.length; i++){ //for loop representing the rows of the array 
      for (int k = 0; k<array[i].length; k++){ //for loop representing the columns of the array 
        System.out.printf("%6d",array[i][k]); //placement to look stacked and print each element of the array 
      }
      System.out.println();
    }
  } //end of display method 
  public static void invade(int[][] array, int k){ //declaring the invade method as void and accepting a 2d int array and integer
    for (int i = 0; i<k; i++){ //for loop running each robot 
      int row = (int)(Math.random()*array.length); //randomly generating a row position in the array 
      int column = (int)(Math.random()*array.length); //randomly generating a column position in the array 
      if (array[row][column] >= 0){ //if the position generated provides a negative element 
        array[row][column] *= (-1); //turn it positive by multiplying by negative 1 
      }
    }
  } //end of invade method 
  public static void update(int[][] array){ //declaring the update method as void and accepting a 2d int array  
    for (int i = 0; i<array.length; i++) { //for loop representing the rows of the array 
      int[] row = array[i]; //creating a smaller array to represent the row
      if(row[row.length-1] <= 0){ //if the length is negative 
        row[row.length-1] *= (-1); //turn it positive by multiplying by negative 1 
      }
      for (int k=row.length-1; k>=0; k--){ //for loop creating an int to represent the length of the row and decrementing it until it is at 0
        if(row[k]<0){ //if the element is negative 
          row[k+1] *= (-1); //turn it positive by multiplying by negative 1 
          row[k] *= (-1); //turn it positive by multiplying by negative 1 
        }
      }
    }
  } //end of update method 
} //end of class 
