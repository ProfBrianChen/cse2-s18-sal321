//This program will use the Scanner class to obtain the original cost of the check, percentage tip, and the number of ways the check will be split. 
import java.util.Scanner; //importing the Scanner class.
public class Check{ //main method for every java program. 
  public static void main(String[] args) { 
    Scanner myScanner = new Scanner( System.in ); //declaring an instance of the scanner object
    System.out.print("Enter the original cost of the check in the form xx.xx: "); //printing what the user sees 
    double checkCost = myScanner.nextDouble(); //allowing input of a value by the user
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): "); //printing what the user sees
    double tipPercent = myScanner.nextDouble(); //allowing input of a value by the user 
    tipPercent /= 100; //converting the percentage into a decimal value. 
    System.out.print("Enter the number of people who went out to dinner: "); //printing what the user sees
    double numPeople = myScanner.nextDouble(); //allowing input of a value by the user
    double totalCost; //creating a variable for the total cost
    double costPerPerson; //creating a variable for the cost per person
    int dollars, dimes, pennies; //creating a variable for the value of the cost with the decimal points as well
    totalCost = checkCost * (1+tipPercent); //calculating the total cost with the tip
    costPerPerson = totalCost / numPeople; //calculating how much each person should pay 
    dollars=(int)costPerPerson; //changing the dollars into an integer variable
    dimes=(int)(costPerPerson * 10) %10; //changing the dimes into an integer variable to go after the decimal place
    pennies=(int)(costPerPerson * 100) %10; //changing the pennies into an integer variable to go after the decimal place
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies); //final print value to show the calculation
  } //end of main method 
} //end of class
