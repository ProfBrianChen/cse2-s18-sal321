//Jane Levin 2/2/18 CSE2 Cyclometer lab, this program will measure a bicycle through the time elapsed in seconds and the number of rotations of the front wheel. 
public class Cyclometer {
  //starting point for all Java programs
  public static void main(String[] args) {
    int secsTrip1=480; //creating an integer value under the variable name of secsTrip1, signifying the time elapsed in seconds during the first trip. 
    int secsTrip2=3220; //creating an integer value under the variable name of secsTrip2, signifying the time elapsed in seconds during the second trip. 
    int countsTrip1=1561; //creating an integer value under the variable name of countsTrip1, signifying the number of rotations of the front wheel during the first trip. 
    int countsTrip2=9037; //creating an integer value under the variable name of countsTrip2, signifying the number of rotations of the front wheel during the second trip. 
    double wheelDiameter=27.0; //creating a double value of the diameter of the wheel as an important constant. 
    double PI=3.14159; //creating a double value of Pi. 
    int feetPerMile=5280; //creating an integer value of the number of feet per mile as an important conversion factor. 
    int inchesPerFoot=12; //creating an integer value of the number of inches per foot as an important conversion factor. 
    int secondsPerMinute=60; //creating an integer value of the number of seconds per minute as an important conversion factor. 
    double distanceTrip1, distanceTrip2, totalDistance; //creating the variables for the values we are looking for. 
    System.out.println("Trip1 took "+
                      (secsTrip1/secondsPerMinute)+" minutes and had "+
                      countsTrip1+" counts."); //printing out the numbers stored in certain variables to calculate the number of minutes and counts that trip 1 had. 
    System.out.println("Trip2 took "+
                      (secsTrip2/secondsPerMinute)+" minutes and had "+
                      countsTrip2+" counts."); //printing out the numbers stored in variables to calculate the number of minutes and counts that trip 2 had.
    int minsTrip1=8; 
    int minsTrip2=53; //After running the calculations, Trip1 took 8 minutes and had 1561 counts. Trip2 took 53 minutes and had 9037 counts. 
    distanceTrip1=countsTrip1*wheelDiameter*PI; //This will determine the distance of Trip1 in inches through multiplying the circumference of the wheel by number of counts. 
    distanceTrip1/=inchesPerFoot*feetPerMile; //Gives the distance of Trip1 in miles. 
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Gives the distance of Trip2 in miles. 
    totalDistance=distanceTrip1+distanceTrip2; //Adding together the two distances to find the total distance. 
    System.out.println("Trip 1 was "+distanceTrip1+" miles."); //printing out the phrase to state the distance of Trip1 in miles. 
    System.out.println("Trip 2 was "+distanceTrip2+" miles."); //printing out the phrase to state the distance of Trip2 in miles. 
    System.out.println("The total distance was "+totalDistance+" miles."); //printing out the phrase to state the distance of both trips combined. 
  } //end of main method
} //end of class