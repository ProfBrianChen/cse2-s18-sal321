//Jane Levin CSE2 Professor Chen Lab09 
//Program that reverses the order and copies various an original array. 
public class lab09{ //class
  public static void main (String[] args) { //main method 
    int [] array0 = {1, 2, 4, 8, 16, 32, 64, 128}; //first array 
    int [] array1 = copy(array0); //calling the copy method for the first array 
    int [] array2 = copy(array0); //calling the copy method for the first array 
    inverter(array0); //reversing the first array 
    print(array0); //printing the first array in reverse 
    System.out.println(" "); 
    inverter2(array1); //reversing the second array again to put it in its original order
    print(array1); //printing the second array normally
    System.out.println(" ");
    int[] array3 = inverter2(array2); //creating a new array that reverses the third array 
    print(array3); //printing the new array in reverse order again 
    System.out.println(" ");
  } //end of main method
  public static int[] copy(int[] originalarray){ //copy method 
    int[] array = new int[originalarray.length]; //new array with the same length as the original array 
    for (int i = 0; i < originalarray.length; i++) { //for loop 
      array[i] = originalarray[i]; //copying the exact elements of the original array for the new array 
    }
    return array; //return statement for the new, copied array 
  } //end of copy method
  public static void inverter(int[] originalarray){ //inverter method
    int num = 0; //declaring and initializing a temp 
    for (int i = 0; i<originalarray.length/2; i++) { //for loop half the length of the original array 
      num = originalarray[originalarray.length - i - 1]; //initializing the temp as the element in the opposite position on the original array 
      originalarray[originalarray.length - i - 1] = originalarray[i]; //middle value will remain the same
      originalarray[i] = num; //element switch places to the opposite side of the array 
    }
  } //end of inverter method 
  public static int[] inverter2(int[] originalarray){ //inverter2 method 
    int[] copyarray = copy(originalarray); //calling the copy method to create a new array that is the same as the original array 
    inverter(copyarray); //reversing the order of the new, copied array 
    return copyarray; //return the reversed, copied array 
  } //end of inverter2 method 
  public static void print(int[] originalarray){ //print method 
    System.out.print("{"); //printing what the user will see 
    for (int i = 0; i<originalarray.length; i++) { //number of times value will be printed 
      System.out.print(originalarray[i] + ", "); //printing each element with a comma 
    }
    System.out.print("}"); //end brackets to end print statement
  } //end of print method 
} //end of class 