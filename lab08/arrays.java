//Jane Levin CSE2 Professor Chen Lab08
//writing a program using arrays to print grades and student names. 
import java.util.Scanner; //importing the scanner class
public class arrays{ 
  public static void main (String [] args){ //main method
    String []students; //declaring the students array 
    Scanner myScanner = new Scanner(System.in); 
    int arrayAmount = (int)(Math.random() *5) + 5; //generating a random number from 0 to 10 to represent the size of the array. 
    students = new String[15]; 
    System.out.print("Enter " + arrayAmount + " strings of student names: "); //printing what the user will see and prompting them. 
    for (int i = 0; i<arrayAmount; i++) { //for loop to represent each string within the array 
      students[i] = myScanner.nextLine(); //initializing each space within the array to a different string. 
    }
    int []midterm; //declaring the midterms array 
    midterm = new int[arrayAmount]; 
    for (int k = 0; k<arrayAmount; k++) { //for loop to represent each midterm grade within the array 
      midterm[k] = (int)(Math.random() *100); //generating a random number from 0 to 100 to represent the grades. 
    }
    System.out.println("Here are the midterm grades for the " + arrayAmount + "students above: "); //printing what the user will see. 
    for (int j = 0; j<arrayAmount; j++) { //for loop to print each string with its corresponding int. 
      System.out.println(students[j] + ": " + midterm[j]); 
    }
    
    
    
  } //end of main method 
} //end of class 