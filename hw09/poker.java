//Jane Levin CSE2 Professor Chen HW09 Poker
//Creating a program that randomly generates an array representing a deck of cards, deals out the cards to each player, and counts up their points
//like a game of poker. Whichever player has the most points wins. 
import java.util.Arrays; //importing arrays
public class poker{ //class
  public static void main (String[] args) { //main method 
    int[] ordercards = random(cards()); //declaring an array within a two methods. 
    int[] player1 = new int[5]; //initializing an array player1 as an integer with 5 elements. 
    int[] player2 = new int[5]; //initializing an array player2 as an integer with 5 elements. 
    for (int i = 0; i<5; i+=2) { //for loop to deal out each hand. 
      player1[i] = ordercards[i]; //allocating the player1 array with the cards
      player2[i] = ordercards[i+1]; //allocating the player2 array with the cards 
    }
    int player1score = 0; //initializing player1score
    int player2score = 0; //initializing player2 score 
    if (pair(player1)){ //if player one has two of a kind 
      player1score += 2; //add two to the score 
    }
    if (pair(player2)){ //if player two has two of a kind 
      player2score += 2; //add two to the score 
    }
    if (threeOfAKind(player1)){ //if player one has three of a kind 
      player1score += 3; //add three to the score 
    }
    if (threeOfAKind(player2)){ //if player two has three of a kind 
      player2score += 3; //add three to the score 
    }
    if (flush(player1)){ //if player one has matching suits 
      player1score += 4; //add four to the score 
    }
    if (flush(player2)){ //if player two has matching suits 
      player2score += 4; //add four to the score
    }
    if (fullHouse(player1)){ //if player one has a full house 
      player1score += 5; //add five to the score 
    }
    if (fullHouse(player2)){ //if player two has a full house 
      player2score += 5; //add five to the score  
    }
    if (player1score > player2score){ //if player one has a higher score than player two 
      System.out.println("Player 1 wins."); //printing who wins 
    } else if (player2score > player1score){ //if player two has a higher score than player one 
      System.out.println("Player 2 wins."); //printing who wins 
    } else { //if they have equal scores 
      System.out.println("Tie."); //printing a tie
    }
  } //end of main method 
  public static int[] cards() { //declaring a cards method 
    int[] deck = new int[52]; //deck array with 52 elements 
    for (int i = 0; i<deck.length; i++){ //for loop to allocate elements to the deck array 
      deck[i] = i;
    }
    return deck; //return statement 
  } //end of cards method 
  public static int[] random(int[] deck){ //declaring a random method to shuffle the cards
    for (int k = 0; k < deck.length; k++){ //for loop to determine 
      int num = (int)(Math.random()*deck.length); //generating random numbers for the elements of the array 
      int temp = deck[k]; //initializing a temp to allocate elements into the deck array 
      while (num != k){ //while the num variable is not equal to k
        deck[k] = deck[num]; //k's spot within the deck array will be the num element of the cards array 
        deck[num] = temp; //the num element in the deck array becomes the new temp 
        break; 
      }
    }
    return deck; //return statement
  }
  public static boolean pair(int[] playerhand){ //pair method to determine if the player has a two of a kind 
    int counter0 = 0;  //declaring and initializing counters for each of the types of cards
    int counter1 = 0; 
    int counter2 = 0;
    int counter3 = 0;
    int counter4 = 0; 
    int counter5 = 0; 
    int counter6 = 0;
    int counter7 = 0; 
    int counter8 = 0; 
    int counter9 = 0;
    int counter10 = 0; 
    int counter11 = 0; 
    int counter12 = 0; 
    for (int i = 0; i< playerhand.length; i++) { //for loop to determine how many times it will run to check every card
      switch(playerhand[i]%13){ //switch statement determining the number of the card 
        case 0:
          counter0++;
        case 1: 
          counter1++;
        case 2:
          counter2++;
        case 3: 
          counter3++;
        case 4:
          counter4++;
        case 5: 
          counter5++;
        case 6:
          counter6++;
        case 7:
          counter7++;
        case 8:
          counter8++;
        case 9:
          counter9++;
        case 10: 
          counter10++;
        case 11:
          counter11++;
        case 12:
          counter12++;
      }
    }
    if (counter0 >= 2 || counter1 >= 2 || counter2 >= 2 || counter3 >= 2 || counter4 >= 2||
          counter5 >= 2 || counter6 >= 2 || counter7 >= 2 || counter8 >= 2 || counter9>= 2 || counter10 >= 2 || counter11 >= 2 || counter12 >= 2){ //if any of the counters equal 2
      return true; //the player has a two of a kind 
    } else {
      return false; //else return false 
    }
  } //end of pair method 
  public static boolean threeOfAKind(int[] playerhand) { //delcaring the three of a kind method 
    int counter0 = 0; //declaring and initializing counters for each number of card
    int counter1 = 0; 
    int counter2 = 0;
    int counter3 = 0;
    int counter4 = 0; 
    int counter5 = 0; 
    int counter6 = 0;
    int counter7 = 0; 
    int counter8 = 0; 
    int counter9 = 0;
    int counter10 = 0; 
    int counter11 = 0; 
    int counter12 = 0; 
    for (int i = 0; i < playerhand.length; i++) { //for loop to check every card
      switch(playerhand[i]%3){ //switch statement to determine the number on each card 
        case 0:
          counter0++;
        case 1: 
          counter1++;
        case 2:
          counter2++;
        case 3: 
          counter3++;
        case 4:
          counter4++;
        case 5: 
          counter5++;
        case 6:
          counter6++;
        case 7:
          counter7++;
        case 8:
          counter8++;
        case 9:
          counter9++;
        case 10: 
          counter10++;
        case 11:
          counter11++;
        case 12:
          counter12++; 
      }
    }
    if (counter0 >= 3 || counter1 >= 3 || counter2 >= 3 || counter3 >= 3 || counter4 >= 3||
          counter5 >= 3 || counter6 >= 3 || counter7 >= 3 || counter8 >= 3 || counter9>= 3 || counter10 >= 3 || counter11 >= 3 || counter12 >= 3){ //if any of the counters equal 3
      return true; //the player has three of a kind 
    } else {
      return false; //else return false 
    }
  } //end of three of a kind method 
  public static boolean flush(int[] playerhand) { //declaring the flush method 
    int spades = 0; //declaring and initializing each suit 
    int hearts = 0; 
    int clubs = 0; 
    int diamonds = 0; 
    for (int i = 0; i < playerhand.length; i++) { //for loop to check every card for its suit 
      switch((int)playerhand[i]/13){ //switch statement to determine the suit of each card 
        case 1: 
          diamonds++;
        case 2: 
          clubs++;
        case 3: 
          hearts++;
        case 4: 
          spades++;
      }
    } //end of flush method 
    if (diamonds == playerhand.length || clubs == playerhand.length || hearts == playerhand.length || spades = playerhand.length){ //if the player has all of the same suit 
      return true; //the player has a flush 
    } else {
      return false; //else return false 
    }
  }
  public static boolean fullHouse(int[] playerhand) { //declaring the full house method 
    if(pair(playerhand) && threeOfAKind(playerhand)){ //if the player has two of a kind and three of a kind 
      return true; //the player has a full house 
    } else {
      return false; //else return false 
    }
  } //end of fullhouse method 
} //end of class 
