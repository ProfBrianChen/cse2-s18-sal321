Grade:     70/100
Comments: 
A) Does the code compile?  How can any compiler errors be resolved?
No. Add parentheses to isolate comparison operations.
B) If the code compiles, does the code run properly?  What kinds of input cause a runtime error?
No. The program gets stuck in a loop.
C) How can any runtime errors be resolved?
Change comparisons so that you do not get caught in an infinite loop.
D) What topics should the student study in order to avoid the errors they made in this homework?
Loops.
E) Other comments:
Incorrect class name. Should be "DrawPoker" not "poker". Think of ways you can use arrays to minimize the amount
of variables you need. Good use of commments. 
