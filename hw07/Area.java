//Jane Levin CSE2 HW07 Professor Chen 
//Calculating the area of three different shapes in different methods depending on the shape that the user choses. 
import java.util.Scanner; //importing the scanner class
public class Area { //start of every Java method 
  public static void main (String[] args) { //main method
    Scanner myScanner = new Scanner(System.in); //declaring scanner class
    System.out.print("Enter the shape you would like: (rectangle, triangle, or circle) "); //printing what the user will see
    while (true) { //while statement to always run 
      if (myScanner.next().equals("rectangle")) { //if statement for if the user inputs rectangle
       double rectlength = CheckInput("Enter the length of your rectangle: ", myScanner); //calling the CheckInput class to enter the dimensions
       double rectwidth = CheckInput("Enter the width of your rectangle: ", myScanner); //calling the CheckInput class to enter the dimensions
       RectangleClass(rectlength, rectwidth); //calling the rectangle area class to determine and print the area
      }
      else if (myScanner.next().equals("triangle")) { //if statement for if the user inputs triangle
        double height = CheckInput("Enter the height of your triangle: ", myScanner); //calling the CheckInput class to enter and check the height
        double length = CheckInput("Enter the length of the base of your triangle: ", myScanner); //calling the CheckInput class to enter and check the length
        TriangleClass(height, length); //calling the triangle area class to determine and print the area
      }
      else if (myScanner.next().equals("circle")) { //if statement for if the user inputs circle
        double radius = CheckInput("Enter the radius of your circle: ", myScanner); //calling the CheckInput class to enter and check the radius
        CircleClass(radius); //calling the circle area class to determine and print the area
      } else { //if the user does not input any of the three shapes. 
        System.out.println("Error: Your input is invalid."); //printing what the user will see in this scenario 
        String junkword = myScanner.next(); //getting rid of the input 
        System.out.print("Enter the shape you would like: (rectangle, triangle, or circle) "); //asking again and continuing in the while loop
    }
  }
  } //end of main method
  public static void RectangleClass(double rectlength, double rectwidth) { //declaring the rectangle area method
    double rectArea = rectwidth * rectlength; //formula for determining the area
    System.out.println("Rectangle Area: " + rectArea); //print statement for the rectangle method
    } //end of rectangle method
  public static void TriangleClass(double height, double length) { //declaring the triangle area method 
    double triarea = (length * height) / 2; //formula for determining the area
    System.out.println("Triangle Area: " + triarea); //print statement for the triangle method
  } //end of triangle method
  public static void CircleClass(double radius) { //declaring the circle area method
    double circlearea = radius * radius * 3.14159265; //formula for determining the area 
    System.out.println("Circle Area: " + circlearea); //print statement for the circle method
  } //end of circle method 
  public static double CheckInput(String statement, Scanner myScanner) { //declaring the method to check the dimensions input
    System.out.print(statement); //printing the statement for the specific shape method asking for a dimension 
    while (!myScanner.hasNextDouble()) { //if the input is not a double
      System.out.println("Error: Your input is invalid." + statement); //printing what the user will see and asking again 
      String junkword = myScanner.next(); //getting rid of the input 
    } 
    double output = myScanner.nextDouble(); //declaring the input if it is a double as the output
    return output; //return to the main method
  } //end of CheckInput 
    } //end of class
