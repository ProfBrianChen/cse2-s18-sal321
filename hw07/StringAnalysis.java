//Jane Levin CSE2 Professor Chen HW07
//Processing a string by analyzing all of the characters. 
import java.util.Scanner; //importing the scanner class
public class StringAnalysis{ //start of class
  public static void main (String[] args){ //main method
    Scanner myScanner = new Scanner(System.in); //declaring scanner
    System.out.print("Enter a string: "); //printing what the user will see first
    String input = myScanner.next(); //initializing the input of the user as a string 
    System.out.print("Press 0 to examine all of the characters. Press 1 to examine a certain number."); //printing what the user sees next.
    int choice = myScanner.nextInt(); //initializing the input as an integer of either 0 or 1 
    if (choice == 0) { //if statement for if the user input a 0
      Accept(input); //calling the Accept class with only a string
    } else if (choice == 1) { //if statement for if the user input a 1
      System.out.print("How many characters would you like to examine? "); //printing what the user will see 
      while(!myScanner.hasNextInt()) { //if the user did not input an integer
        System.out.print("Error: You did not input an integer."); //the system will print an error statement. 
        String junkword = myScanner.next(); //getting rid of the incorrect input
        System.out.print("How many characters would you like to examine? "); //asking the question again 
      }
      int j = myScanner.nextInt(); //declaring j as an integer and to equal the integer that the user inputs on how many chars they want to analyze. 
      Accept(input, j); //calling the Accept class with a string and int 
    }
  } //end of main method
  public static void Accept(String input){ //declaring a method 'Accept' for if the user chooses to analyze their entire string. 
   int i = input.length(); //declaring i as the length of the input of the user
   int z = 0; //declaring and initializing a variable z with a value of 0 to compare to i 
   boolean letters = true; //declaring and initializing a boolean as true
   while ((letters == true) && (z<i)) { //while statement for when the boolean is true and when the z literal is less than the inputted length by the user. 
     char currentChar = input.charAt(z); //specifically refering to the current char that the program is analyzing
     if ((currentChar >= 'a') && (currentChar <= 'z')) { //if statement to determine whether or not the char is a letter/ between a and z
       letters = true; //the boolean will still be true if the char is a letter. 
       z++; //increment of z to move onto the next char. 
     } else { 
       letters = false; //the boolean will be false if the char is not proven to be a letter. 
     }
   }
   if (letters == true) { //if statement for if the boolean still remains true after going through all of the chars. 
     System.out.println("All characters are letters."); //final print statement to analyze the string. 
   } else { //if boolean is false 
     System.out.println("Not all characters are letters."); //final print statement to analyze the string. 
   }
  } //end of first Accept method 
  public static void Accept(String input, int j){ //declaring a method 'Accept' for if the user chooses to analyze only a certain number of chars in their string. 
    int i = input.length(); //declaring i as the length of the input of the user. 
    if (j>i) { //if statement for if the number of chars the user wants to analyze is greater than the length of the string. 
      j = i; //j just becomes the length of the statement
    } else { //if the number of chars the user wants to analyze is less than the length of the string. 
      i = i; //i will stay as its own value. 
    }
    int z = 0; //declaring and initializing a variable z with a value of 0 to compare to the amount of chars the user wants to analyze. 
    boolean letters = true; //declaring and initializing a boolean as true. 
    while ((letters ==true) && (z<j)) { //whlie statement for when the boolean is true and when the z literal is less than the number of chars to be analyzed. 
      char currentChar = input.charAt(z); //specifically refering to the current char that the program is analyzing. 
      if ((currentChar >= 'a') && (currentChar <= 'z')) { //if statement to determine whether or not the char is a letter/ between a and z
        letters = true; //the boolean will remain true if the char is a letter. 
        z++; //increment of z to move onto the next char. 
      } else { 
        letters = false; //the boolean will be false if the char is not proven to be a letter. 
      }
    }
    if (letters == true) { //if statement for if the boolean still remains true after going through all of the chars. 
      System.out.println("All examined characters are letters."); //final print statement to analyze the string. 
    } else if (letters == false) { //if the boolean is false after going through all of the chars. 
      System.out.println("Not all examined characters are letters."); //final print statement to analyze the string. 
    }
  } //end of second Accept method 
} //end of class
