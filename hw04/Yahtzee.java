// Jane Levin CSE2 Professor Chen HW04, developing a game of Yahtzee
import java.util.Scanner; //importing the scanner class
public class Yahtzee { //main method for every java program
  public static void main(String[] args) {
    Scanner myScanner = new
      Scanner(System.in);
    System.out.print("If you'd like a random roll, select 2. If you would like to select your own numbers, select 1: "); //printing what the user will see
    double answerRoll = myScanner.nextDouble(); //initializing their input as a variable
    int dieOne = 0; //initializing each die as an integer with 0 value. 
    int dieTwo = 0;
    int dieThree = 0;
    int dieFour = 0;
    int dieFive = 0;
    int program = 0;
    if (answerRoll !=1 && answerRoll !=2) { //creating an if statement for is the user doesn't input a 1 or 2. 
      System.out.println("Error: you did not enter a valid option."); //printing what the user will see if they don't input a 1 or 2.
    }
    if (answerRoll == 1) {  //creating an if statement if the user wants to choose their own roll. 
      System.out.print("Enter your roll for the first die: "); //printing what the user will see if they want to put in their own numbers for the roll. 
      int dieOneChoice = myScanner.nextInt(); //declaring the choice as an integer
      if (dieOneChoice>=7 || dieOneChoice==0){ //creating an if statement if the user does not input an integer from 1 to 6. 
        System.out.println("Error: you did not enter a valid roll."); //printing what the user will see if they don't input an integer from 1 to 6. 
      } else if (dieOneChoice!=0 || dieOneChoice<=6) { //creating an if statement if the user types an integer from 1 to 6. 
      dieOne = dieOne + dieOneChoice; //initializing dieOne as the choice of the user. 
      System.out.print("Enter your roll for the second die: ");
      int dieTwoChoice = myScanner.nextInt();
      if (dieTwoChoice>=7 || dieTwoChoice==0){
        System.out.println("Error: you did not enter a valid roll.");
      } else if (dieTwoChoice!=0 || dieTwoChoice<=6) {
      dieTwo = dieTwo + dieTwoChoice;
      System.out.print("Enter your roll for the third die: ");
      int dieThreeChoice = myScanner.nextInt();
      if (dieThreeChoice>=7 || dieThreeChoice==0){
      System.out.println("Error: you did not enter a valid roll.");
      } else if (dieThreeChoice!=0 || dieThreeChoice<=6) {
      dieThree = dieThree + dieThreeChoice;
      System.out.print("Enter your roll for the fourth die: ");
      int dieFourChoice = myScanner.nextInt();
      if (dieFourChoice>=7 || dieFourChoice==0){
      System.out.println("Error: you did not enter a valid roll.");
      } else if (dieFourChoice!=0 || dieFourChoice<=6) {
      dieFour = dieFour + dieFourChoice;
      System.out.print("Enter your roll for the fifth die: ");
      int dieFiveChoice = myScanner.nextInt();
      if (dieFiveChoice>=7 || dieFiveChoice==0){
      System.out.println("Error: you did not enter a valid roll.");
      } else if (dieFiveChoice!=0 || dieFiveChoice<=6) {
      dieFive = dieFive + dieFiveChoice;
      }
      }
      }
      }
      }
      }
    if (answerRoll == 2) { //generating 5 random numbers between 1 and 6 for the dice if the user chose option 2. 
      dieOne = (int)(Math.random() * (6)) + 1;
      dieTwo = (int)(Math.random() * (6)) + 1;
      dieThree = (int)(Math.random() * (6)) + 1;
      dieFour = (int)(Math.random() * (6)) + 1;
      dieFive = (int)(Math.random() * (6)) + 1;
    }
      int numberOnes = 0; //initializing variables for the number of values on each die. 
      int numberTwos = 0;
      int numberThrees = 0;
      int numberFours = 0;
      int numberFives = 0;
      int numberSixes = 0;
      switch (dieOne) { //switch statement to count up the value on die 1.
        case 1: numberOnes = numberOnes + 1;
          break;
        case 2: numberTwos = numberTwos + 1;
          break;
        case 3: numberThrees = numberThrees + 1;
          break;
        case 4: numberFours = numberFours + 1;
          break;
        case 5: numberFives = numberFives + 1;
          break;
        case 6: numberSixes = numberSixes + 1;
          break;
      }
      switch(dieTwo) { //switch statement to count up the value on die 2. 
        case 1: numberOnes = numberOnes + 1;
          break;
        case 2: numberTwos = numberTwos + 1;
          break;
        case 3: numberThrees = numberThrees + 1;
          break;
        case 4: numberFours = numberFours + 1;
          break;
        case 5: numberFives = numberFives + 1;
          break;
        case 6: numberSixes = numberSixes + 1;
          break;
      }
      switch(dieThree) { //switch statement to count up the value on die 3. 
        case 1: numberOnes = numberOnes + 1;
          break;
        case 2: numberTwos = numberTwos + 1;
          break;
        case 3: numberThrees = numberThrees + 1;
          break;
        case 4: numberFours = numberFours + 1;
          break;
        case 5: numberFives = numberFives + 1;
          break;
        case 6: numberSixes = numberSixes + 1;
          break;
      }
      switch(dieFour) { //switch statement to count up the value on die 4. 
        case 1: numberOnes = numberOnes + 1;
          break;
        case 2: numberTwos = numberTwos + 1;
          break;
        case 3: numberThrees = numberThrees + 1;
          break;
        case 4: numberFours = numberFours + 1;
          break;
        case 5: numberFives = numberFives + 1;
          break;
        case 6: numberSixes = numberSixes + 1;
          break;
      }
      switch(dieFive) { //switch statement to count up the value on die 5. 
        case 1: numberOnes = numberOnes + 1;
          break;
        case 2: numberTwos = numberTwos + 1;
          break;
        case 3: numberThrees = numberThrees + 1;
          break;
        case 4: numberFours = numberFours + 1;
          break;
        case 5: numberFives = numberFives + 1;
          break;
        case 6: numberSixes = numberSixes + 1;
          break;
      }
      int upperAces = numberOnes; //declaring the aces as the number of ones. 
      int upperTwos = numberTwos * 2; //declaring the twos as the number of twos multiplied by two. 
      int upperThrees = numberThrees * 3; //declaring the threes as the nunmber of threes multiplied by three. 
      int upperFours = numberFours * 4; //declaring the fours as the number of fours multiplied by four. 
      int upperFives = numberFives * 5; //declaring the fives as the number of fives multiplied by five. 
      int upperSixes = numberSixes * 6; //declaring the sixes as the number of sixes multiplied by six. 
      int upperTotal = upperAces + upperTwos + upperThrees + upperFours + upperFives + upperSixes; //adding together all of the upper section values. 
      int upperTotalBonus = upperTotal; //initializing the upper section total with bonus. 
      if (upperTotal >= 63) { //creating an if statement to add the value of bonus to the upper section if it is greater or equal to 63. 
        upperTotalBonus += 35;
      }
      boolean twoOfAKind = false; //initializing the variables for the lower section values. 
      int threeOfAKind = 0;
      int fourOfAKind = 0;
      int fullHouse = 0;
      int smStraight = 0;
      int lgStraight = 0;
      int chance = 0;
      int yahtzee = 0;
      int similar = 0;
      int totalDice = dieOne + dieTwo + dieThree + dieFour + dieFive;
      if (numberOnes == 2) { //counting whether or not there is two of a kind for the full house points later on. 
        twoOfAKind = true;
      } else if (numberTwos == 2) {
        twoOfAKind = true;
      } else if (numberThrees == 2) {
        twoOfAKind = true;
      } else if (numberFours == 2) {
        twoOfAKind = true;
      } else if (numberFives == 2) {
        twoOfAKind = true;
      } else if (numberSixes == 2) {
        twoOfAKind = true;
      }
      if (numberOnes >= 3) { //counting up whether there is three of a kind for all of the dice and adding the total values. 
        threeOfAKind = threeOfAKind + totalDice;
      } else if (numberTwos >=3) {
        threeOfAKind = threeOfAKind + totalDice;
      } else if (numberThrees >= 3) {
        threeOfAKind = threeOfAKind + totalDice;
      } else if (numberFours >= 3) {
        threeOfAKind = threeOfAKind + totalDice;
      } else if (numberFives >= 3) {
        threeOfAKind = threeOfAKind + totalDice;
      } else if (numberSixes >= 3) {
        threeOfAKind = threeOfAKind + totalDice;
      } 
      if (numberOnes >= 4) { //counting up whether there is four of a kind for all of the dice and adding the total values. 
        fourOfAKind = fourOfAKind + totalDice;
      } else if (numberTwos >=4) {
        fourOfAKind = fourOfAKind + totalDice;
      } else if (numberThrees >= 4) {
        fourOfAKind = fourOfAKind + totalDice;
      } else if (numberFours >= 4) {
        fourOfAKind = fourOfAKind + totalDice;
      } else if (numberFives >= 4) {
        fourOfAKind = fourOfAKind + totalDice;
      } else if (numberSixes >= 4) {
        fourOfAKind = fourOfAKind + totalDice;
      } 
      if (numberOnes == 5) { //counting up whether there is a yahtzee for the dice and adding 50 points. 
        yahtzee = yahtzee + 50;
      } else if (numberTwos ==5) {
        yahtzee = yahtzee + 50;
      } else if (numberThrees == 5) {
        yahtzee = yahtzee + 50;
      } else if (numberFours == 5) {
        yahtzee = yahtzee + 50;
      } else if (numberFives == 5) {
        yahtzee = yahtzee + 50;
      } else if (numberSixes == 5) {
        yahtzee = yahtzee + 50;
      }
      if (threeOfAKind > 0 && twoOfAKind == true) { //if there is a three of a kind and also a two of a kind, then the full house points are added up. 
        fullHouse = fullHouse + 25;
      }
      if (numberOnes > 0 && numberTwos > 0 && numberThrees > 0 && numberFours > 0) { //adding up whether or not there is a small straight (3 different ways)
        smStraight = smStraight + 30;
      } else if (numberTwos > 0 && numberThrees > 0 && numberFours > 0 && numberFives > 0) {
        smStraight = smStraight + 30;
      } else if (numberThrees > 0 && numberFours > 0 && numberFives > 0 && numberSixes > 0) {
        smStraight = smStraight + 30;
      }
    if (numberOnes > 0 && numberTwos > 0 && numberThrees > 0 && numberFours > 0 && numberFives > 0) { //adding up whether or not there is a large straight (2 different ways)
        lgStraight = lgStraight + 40;
            }
    if (numberTwos > 0 && numberThrees > 0 && numberFours > 0 && numberFives > 0 && numberSixes > 0) {
        lgStraight = lgStraight + 40;
            }
    chance = totalDice; //calculating the chance value of each roll. 
    int lowerTotal = threeOfAKind + fourOfAKind + smStraight + lgStraight + fullHouse + yahtzee + chance; //adding up the lower total values of all variables. 
    int grandTotal = upperTotalBonus + lowerTotal; //adding up the lower and upper section values to get the grand total. 
      System.out.println("You rolled: " +dieOne+" "+dieTwo+" "+dieThree+" "+dieFour+" "+dieFive); //printing what the user will see through the entire game. 
    System.out.println("Upper section without bonus: "+upperTotal);
    System.out.println("Upper section total with bonus: "+upperTotalBonus);
    System.out.println("Lower section total: "+lowerTotal);
    System.out.println("Grand total: "+grandTotal); //printing the grand total. 
  } //end of main method
} //end of class